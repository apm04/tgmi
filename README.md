# tgmi

tgmi is a linux python script implementing a command line browser for the gemini protocol with a scrollable page for display of *gmi files with link-type and header coloring.  tgmi implements:

* site bookmarking
* server TOFU authentication
* client certificates
* titan protocol file uploads
* automatic handler detection and startup for downloaded files
* automatic transfer of chosen http/https links to a designated browser
* auto conversion of an atom feed xml file into a displayable gemtext page

tgmi heavily relies on two python packages prompt-toolkit by Jonathan Slenders and Agunua by Stéphane Bortzmeyer. The program has been tested for compatibility with debian, ubuntu and Windows 10/11 in Windows Subsystem for Linux (WSL or WSL2).

### Installation Requirements (Linux)/Windows 10/11 WSL/WSL2

1. a current debian or ubuntu installation (or a derivative thereof)  
2. Python 3.7+ (satisfied from a normal install of 1. above)  
3. python3-distutils is installed  
4. xdg-utils is installed
5. PyOpenSSL         from pypi, the package name is 'pyopenssl'
6. Agunua            from pypi, the package name is 'agunua'  
7. prompt-toolkit    from pypi, the package name is 'prompt-toolkit'

### Usage

Refer to the file SETUP.txt. After installing required software packages, uncompress tgmi.zip into a folder at a convenient location. From a terminal navigate into that folder and run: 'python3 tgmi.py'. At the command line type 'h' to view a help dialog listing commands or type H for a more expansive help file. For Windows 10/11 WSL/WSL2, it is convenient to place the folder tgmi in C:\Users\[user]\Documents\  This path from inside the WSL terminal is denoted as /mnt/c/users/[user]/Documents/[tgmi folder].

### License

GPL v.2  See LICENSE.txt.

### Recent changes

See the file CHANGES.txt.

### Screencap1 - illustrating screen and color coding of link types
![SCREENCAP1](/uploads/f55daa25420a51123cf7ec2859faa970/SCREENCAP1.png)

### Screencap2 - site information from Agunua url call
![SCREENCAP2](/uploads/7cd65ae319c31f1f22926a3c7f10d2ee/SCREENCAP2.png)

### Screencap3 - quick help dialog
![SCREENCAP3](/uploads/428189d77cd004c40ff276157441bd28/SCREENCAP3.png)

### Screencap4 - binary download with handler
![SCREENCAP4](/uploads/5e3afb6ccd7b3337c981c5d783c30c76/SCREENCAP4.png)

### Authors

Peter Maika <apm04@hotmail.com>

## Reference site

https://framagit.org/apm04/tgmi
