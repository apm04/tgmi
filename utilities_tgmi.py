#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# utility functions for use by tgmi
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

# import global names
import cfg

import cgi
import os, sys, platform, glob, re, configparser, time
import subprocess, shutil
import textwrap
import urllib.parse
import xml.etree.ElementTree as ET
import inspect


# set path locations
def set_paths_and_cleanup():
	# determine the operating system
	os_type = platform.system()
	cfg.OS_IS_LINUX = True if os_type=='Linux' else False
	
	# if the string 'Microsoft' is in the uname, we are using WSL
	# if the string 'microsoft' is in the uname, we are using WSL2
	result = subprocess.check_output(['uname', '-a']).decode('utf-8')
	if ('icrosoft' in result):
		cfg.OS_IS_LINUX = False
	
	# find out where program is located
	cfg.APP_DIR = os.path.dirname(os.path.abspath(__file__))
	
	# path to bookmarks, help, welcome
	cfg.SUPP = cfg.APP_DIR + '/supp/'
	# path to settings directory
	cfg.SETTINGS = cfg.SUPP + 'settings/'
	# path to temporary directory for downloaded files
	cfg.TEMP = cfg.SUPP + 'temp/'
	# path to Agunua fingerprints - TOFU storage for server certificates
	cfg.TOFU = cfg.SUPP + 'fingerprints/'
	# path to logs
	cfg.LOGS = cfg.SUPP + 'logs/'
	# path to certificates
	cfg.CERTS = cfg.SUPP + 'certs/'

	# overwrite old logfile.txt with 0-character file
	with open(cfg.LOGS + 'logfile.txt', 'w') as f:
		f.write('')
		
	# clean up cfg.TEMP in case we had a previous bad exit
	oldfiles = glob.glob(cfg.TEMP + '*')
	for f in oldfiles:
		subprocess.call(['rm', f])
		
	return


# read settings.conf, create with default values if it does not exist
def read_settings():
	settings_path = cfg.SETTINGS + 'settings.conf'
	# if settings.conf does not exist create it populated with default values
	if not os.path.exists(settings_path):
		if cfg.OS_IS_LINUX:
			app_text  = get_app('text/plain')
			app_https = get_app('x-scheme-handler/https')
			
			config = configparser.ConfigParser()
			config['Settings'] = {
				'server timeout in seconds'					: '8',
				'tab size'                                : '4',
				'downloaded files default path'				: os.getenv('HOME') + '/Downloads/',
				'display line width'								: '80',
				'application to use for text files'			: app_text,
				'browser to use for http and https urls'	: app_https,
				'IPv4 connections only'							: 'False',
				}
				
		else:
			indices = [i for i, x in enumerate(cfg.APP_DIR) if x == '/']
			win_dlpath = cfg.APP_DIR[:indices[4]] + '/Downloads/'
			
			config = configparser.ConfigParser()
			config['Settings'] = {
				'server timeout in seconds'					: '8',
				'tab size'                                : '4',
				'downloaded files default path'				: win_dlpath,
				'display line width'								: '80',
				'application to use for text files'			: '/mnt/c/Windows/notepad.exe',
				'browser to use for http and https urls'	: '/mnt/c/Program Files (x86)/Microsoft/Edge/Application/msedge_proxy.exe',
				'IPv4 connections only'							: 'False',
				}
	
		with open(settings_path, 'w') as configFile:
			config.write(configFile)
		
	# read the settings.conf file
	config = configparser.ConfigParser()
	config.read(settings_path)

	cfg.SERVER_TIMEOUT	= config['Settings'].getint('server timeout in seconds')
	cfg.TAB					= config['Settings'].getint('tab size')
	cfg.PERM_DLPATH		= config['Settings']['downloaded files default path']
	cfg.DISPLAY_WIDTH		= config['Settings'].getint('display line width')
	cfg.HANDLER_TEXT		= config['Settings']['application to use for text files']
	cfg.HANDLER_WWW		= config['Settings']['browser to use for http and https urls']
	cfg.FLAG_IPV4_ONLY	= config['Settings'].getboolean('IPv4 connections only')
	return


# read auto_send_certs.txt into cfg.AUTOSENDCERTS, create if it does not exist
def read_autosendcerts():
	autosendcerts_path = cfg.CERTS + 'auto_send_certs.txt'
	# if auto_send_certs.txt does not exist, create an empty file
	if not os.path.exists(autosendcerts_path):
		open(autosendcerts_path, 'a').close()
		cfg.AUTOSENDCERTS = ''
		cfg.PARSED_ASC = []
	else:
		with open(autosendcerts_path, 'r') as f:
			cfg.AUTOSENDCERTS = f.read()
		f.close()
		cfg.PARSED_ASC = cfg.AUTOSENDCERTS.split('\n')


# if necessary, revise auto_send_certs.txt, cfg.AUTOSENDCERTS and cfg.PARSED_ASC
# if there has been a new certificate created
def redo_autosendcerts(fIncludeDir, cert_path):
	autosendcerts_path = cfg.CERTS + 'auto_send_certs.txt'
	if fIncludeDir:
		# we want the directory included in autosendcerts.txt
		if (cert_path not in cfg.PARSED_ASC):
			with open(cfg.CERTS + 'auto_send_certs.txt', 'a') as f:
				f.write(cert_path + '\n')
			f.close()
			read_autosendcerts()

	else:
		# if it was previously there, remove the directory
		# from cfg.AUTOSENDCERTS, cfg.PARSED_ASC and auto_send_certs.txt
		# otherwise do nothing
		if (cert_path in cfg.PARSED_ASC):
			newauto = ''
			autosends = cfg.PARSED_ASC
			for token in autosends:
				if ((cert_path not in token) and (len(token) > 0)):
					newauto += token + '\n'
			cfg.AUTOSENDCERTS = newauto
			# store the updated file
			os.remove(cfg.CERTS + 'auto_send_certs.txt')
			with open(cfg.CERTS + 'auto_send_certs.txt', 'a') as f:
				f.write(cfg.AUTOSENDCERTS)
			f.close()
			cfg.PARSED_ASC = cfg.AUTOSENDCERTS.split('\n')


def isotime(datestr):
	# converts time from 'openssl ... -dates' call into ISO format 
	months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', \
				'Sep', 'Oct', 'Nov', 'Dec']
	isostr = datestr[25:29]
	isostr += '/' + f'{int(months.index(datestr[9:12])+1):02d}'
	isostr += '/' + f'{int(datestr[14:15]):02d}'
	isostr += datestr[15:24] + ' GMT'
	return isostr


# generate and store a crt/key pair in supp/certs/{cert_url}
def create_permanent_cert(cert_url, issuer, term):
	parsed_cert_url = urllib.parse.urlparse(cert_url)
	surl = parsed_cert_url.netloc + parsed_cert_url.path
	surl = cert_url.replace('/', '@@')
	certdir = cfg.CERTS + surl + '/'
	if os.path.exists(certdir):
		return 1, ('Directory for crt/key pair exists, did you mean \'Replace\'?')
	else:
		if os.path.exists(cfg.TEMP + 'sscertificate.crt'):
			os.remove(cfg.TEMP + 'sscertificate.crt')
		if os.path.exists(cfg.TEMP + 'sscertificate.key'):
			os.remove(cfg.TEMP + 'sscertificate.key')
		output = subprocess.run(['bash', 'createcertpem.sh', '/CN='+issuer, str(term)], capture_output=True)
		str_array = (output.stdout.decode()).split('\n')
		os.mkdir(certdir)
		shutil.move(cfg.TEMP + 'sscertificate.crt', certdir + 'sscertificate.crt')
		shutil.move(cfg.TEMP + 'sscertificate.key', certdir + 'sscertificate.key')

		return 0, ('certificate created, valid to ' + isotime(str_array[2]))


# generate an ephemeral certificate
def generate_ephcert(issuer, term):
	if os.path.exists(cfg.TEMP + 'sscertificate.crt'):
		os.remove(cfg.TEMP + 'sscertificate.crt')
	if os.path.exists(cfg.TEMP + 'sscertificate.key'):
		os.remove(cfg.TEMP + 'sscertificate.key')

	output = subprocess.run(['bash', 'createcertpem.sh', '/CN='+issuer, str(term)], capture_output=True)
	str_array = (output.stdout.decode()).split('\n')

	return 0, ('certificate created in supp/temp/  valid to ' + isotime(str_array[2]))


# create custom startup screen sized to terminal screen
def create_welcome_screen(gmiboxheight):
	
	str2 =  " _                      _ "
	pad = ' '*int((cfg.DISPLAY_WIDTH - len(str2))/2)
	str1 = '```\n'
	str1 += pad + str2 + '\n'
	str1 += pad + "| |                    (_)" + '\n'
	str1 += pad + "| |_   __ _  _ __ ___   _ " + '\n'
	str1 += pad + "| __| / _` || '_ ` _ \ | |" + '\n'
	str1 += pad + "| |_ | (_| || | | | | || |" + '\n'
	str1 += pad + " \__| \__, ||_| |_| |_||_|" + '\n'
	str1 += pad + "       __/ |              " + '\n'
	str1 += pad + "      |___/               " + '\n'
	str1 += pad + "                          " + '\n'
	str1 += '```\n'
	str1 += '\n'
	str2 = 'Welcome to ' + cfg.PNAME + ' ' + cfg.PVERSION + ' (' + cfg.PDATE + '), a Gemini protocol terminal browser'
	pad = ' '*int((cfg.DISPLAY_WIDTH - len(str2))/2)
	str1 += pad + str2 + '\n\n' 
	str2 = 'Enter gemini URL, B (bookmarks), h (command list), H (all help), q (quit)'
	pad = ' '*int((cfg.DISPLAY_WIDTH - len(str2))/2)
	str1 += pad + str2 + '\n'
	
	linends = [pos for pos, char in enumerate(str1) if char == '\n']
	nlines = len(linends) - 2
	
	top_lines = int((gmiboxheight - nlines)/2)
	vpad_top = ('\n')*top_lines
	vpad_bottom = ('\n')*(gmiboxheight - nlines - top_lines)
	str1 = vpad_top + str1 + vpad_bottom
	str1 = str1[:-1]
	
	if os.path.exists(cfg.SUPP + 'welcome.gmi'):
		subprocess.call(['rm', cfg.SUPP + 'welcome.gmi'])
		
	f = open(cfg.SUPP + 'welcome.gmi', 'w')
	f.write(str1)
	f.close()


# convert a linux path to file to windows path
def make_winpath(path):
	str1 = re.sub('/mnt/c', 'C:', path)
	str1 = re.sub('/', '\\\\', str1)
	return str1
	

# get windows app name from full path to app
def get_win_name(name):
	if cfg.OS_IS_LINUX:
		return name
	else:
		index = name.rfind('/')
		if (index==-1):
			return name
		else:
			return name[index+1:]


# return string with ANSI codes to print as colored
def cstr(str_print, col='red'):
	"""
	return a string with leading escape sequences to print colors on terminal
	color is one of: 'black', 'red', 'green', 'brown', 'blue', 'magenta', 'cyan', 'gray'
	optional weight decorator preceding color is one of: 'n' ,'b', 'l', 'i', 'u'
		'n'=normal  'b'=bold  'l'=light  'i'=italic  'u'=underline
	default: col='red' == red normal weight
	"""
	try:	
		c = ['black', 'red', 'green', 'brown', 'blue', 'magenta', 'cyan', 'gray'].index(col)
		w = 0
	except ValueError:
		c = ['black', 'red', 'green', 'brown', 'blue', 'magenta', 'cyan', 'gray'].index(col[1:])
		w = 'nbliu'.index(col[0])
	colseq = '\033[' + str(w) + ';3' + str(c) + 'm'
	return (colseq + str(str_print) + '\033[0m')
	
# find the default application for a mimetype
def get_app(mimetype):
	result = subprocess.check_output(['xdg-mime', 'query', 'default', mimetype]).decode('utf-8')
	if ('command not found' in result):
		return '*none*'
	else:
		app = result.split('.')[0].lower()
		app = app.replace('-1', '')
		app = '*none*' if len(app)==0 else app
		return app
		

class ServerTimeoutError(Exception):
	pass


# timeout function
def server_timeout(signum, frame):
	raise ServerTimeoutError


# change relative links to full
def absolutise_url(base, relative):
	#log_to_file('absolutize[219]: base: ' + base + '   relative: ' + relative)
	# tgmi 1.1.4 fix for republic.circumlunar.space/users
	if (base == 'gemini://republic.circumlunar.space/users'):
		base += '/'
	# check if we are dealing with a local windows .gmi file
	if ((not cfg.OS_IS_LINUX) and (base[:7]=='file://') and ('://' not in relative)):
		index = base.rfind('\\')
		base = base[:index]
		return base + '\\' + relative
	
	if "://" not in relative:
		# Python's URL tools somehow only work with known schemes?
		base = base.replace("gemini://","http://")
		# if there is an index.gmi file sometimes this is not added to the server url
		# in this case, other files in the same directory will not be able to be accessed
		# unless a trailing '/' is added to the path
		if ((base[-1] != '/') and ('.gmi' not in base)):
				base += '/'
		relative = urllib.parse.urljoin(base, relative)
		#log_to_file('utils[320]: relative: ' + relative)
		relative = relative.replace("http://", "gemini://")

	#log_to_file('            relative: ' + relative + '\n')
	return relative


# paragraph print
def pr_line(line, lexercode, width):
	str1 = ''
	lcode = lexercode
	wrapper = textwrap.TextWrapper(width=width)
	word_lines = wrapper.wrap(line)
	for ln in word_lines:
		str1 = lcode + ln + '\n'
		cfg.lc += 1
	return str1


# long line print with optional indent and special first and following prefix
def pr_longline(line, lexercode, first='', follow='', indent=0):
	str1 = ''
	lcode = lexercode
	dwidth = cfg.DISPLAY_WIDTH
	if (len(follow) == 0):
		follow = ' '*len(first)
		
	if (len(line) <= dwidth - 2*indent - len(first)):
		str1 += lcode + ' '*indent + first + line + '\n'
		cfg.lc += 1
	else:
		wrapper = textwrap.TextWrapper(width=dwidth - 2*indent - len(first))
		word_lines = wrapper.wrap(line)
		str1 += lcode + ' '*indent + first + word_lines[0] + '\n'
		cfg.lc += 1
		for ln in word_lines[1:]:
			str1 += lcode + ' '*indent + follow + ln + '\n'
			cfg.lc += 1
			
	return str1


# long line print for ExtendedInfo
def pr_Exlongline(line, first=''):
	str1 = ''
	if (line == None):
		return str1
	else:
		line = str(line)
	follow = ' '*len(first)
	
	if (len(line) <= cfg.DISPLAY_WIDTH - len(first)):
		str1 += first + line + '\n'
		cfg.lc += 1
	else:
		wrapper = textwrap.TextWrapper(width=cfg.DISPLAY_WIDTH - len(first))
		word_lines = wrapper.wrap(line)
		str1 += first + word_lines[0] + '\n'
		cfg.lc += 1
		for ln in word_lines[1:]:
			str1 += follow + ln + '\n'
			cfg.lc += 1
			
	return str1


def ExtendedInfo(url, u):
	cfg.lc, str1 = 0, ''
	str1 += '# Info for URL: ' + url + '\n'
	cfg.lc += 1
	try:
		str1 += pr_Exlongline(u.header,         'u.header            :  ')
	except:
		pass
	str1 += pr_Exlongline(u.network_success,   'u.network_success   :  ')
	if (u.status_code is not None):
		str1 += 'u.status_code       :  ' + u.status_code + '          [' + cfg.gemcodes[u.status_code] + ']' + '\n'
		cfg.lc += 1
	str1 += pr_Exlongline(u.error,             'u.error             :  ')
	str1 += pr_Exlongline(u.meta,              'u.meta              :  ')
	str1 += pr_Exlongline(u.ip_address,        'u.ip_address        :  ')
	str1 += pr_Exlongline(u.links,             'u.links             :  ')
	if (u.status_code == '20'):
		str1 += '\n\n'
		str1 += '## extra info when status code is 20:' + '\n'
		cfg.lc += 1
		try:
			str1 += pr_Exlongline(u.binary,         'u.binary            :  ')
		except:
			pass
		try:
			str1 += pr_Exlongline(u.size,           'u.size (body size)  :  ')
		except:
			pass
		try:
			str1 += pr_Exlongline(u.mediatype,      'u.mediatype         :  ')
		except:
			pass
		try:
			str1 += pr_Exlongline(u.lang,           'u.lang              :  ')
		except:
			pass
		try:
			str1 += pr_Exlongline(u.charset,        'u.charset           :  ')
		except:
			pass
		try:
			str1 += pr_Exlongline(u.no_shutdown,    'u.no_shutdown       :  ')
		except:
			pass
		
	if (u.network_success == True):
		str1 += '\n'
		str1 += '## server certificate info:' + '\n'
		cfg.lc += 1
		str1 += pr_Exlongline(u.tls_version,    'u.tls_version       :  ')
		str1 += pr_Exlongline(u.issuer,         'u.issuer            :  ')
		str1 += pr_Exlongline(u.subject,        'u.subject           :  ')
		str1 += pr_Exlongline(u.expired,        'u.expired           :  ')
		str1 += pr_Exlongline(u.cert_not_after, 'u.cert_not_after    :  ')
		str1 += pr_Exlongline(u.cert_not_before,'u.cert_not_before   :  ')
		str1 += pr_Exlongline(u.cert_algo,      'u.cert_algo         :  ')
		str1 += pr_Exlongline(u.cert_key_type,  'u.cert_key_type     :  ')
		str1 += pr_Exlongline(u.keystring,      'u.keystring         :  ')
		str1 += pr_Exlongline(u.cert_key_size,  'u.cert_key_size     :  ')
		
	return str1
	

def prelexer(body, mime, url, fExtendedInfo):
	cfg.lc = 0
	#fheader1 = False
	h1_index = 0
	bbody = ''

	# Decode according to declared charset
	mime, mime_opts = cgi.parse_header(mime)
	if ((not fExtendedInfo) and not isinstance(body, str)):
		body = body.decode(mime_opts.get("charset","UTF-8"))

	parsed_url = urllib.parse.urlparse(url)

	# filter out all utf-8 codepoints known to cause terminal display issues
	body = filter_utf8(body)
	
	# tgmi 1.1.2 if body is only one character, prepend with one space
	# so a blank page will be displayed
	if (len(body) == 1):
		body = ' ' + body

	# Handle a Gemini map
	if mime == "text/gemini":
		cfg.menu = []
		fPreformatted = False
		h1_index = -1
		for i,line in enumerate(body.splitlines()):
			
			# if line after header1 line is \n skip since underline gives spacing
			if ((len(line)==0) and (i==h1_index+1)):
				pass
			
			# preformatted lines
			elif line.startswith("```"):
				fPreformatted = not fPreformatted
				
			elif fPreformatted:
				line = textwrap.fill(line, width=999, expand_tabs=True, tabsize=cfg.TAB, replace_whitespace=False)
				bbody += pr_longline(line, 'A', '', '')
				cfg.lc += 1
				
			# link lines
			elif line.startswith("=>") and line[2:].strip():
				bits = line[2:].strip().split(maxsplit=1)
				link_url = bits[0]
				link_url = absolutise_url(url, link_url)
				cfg.menu.append(link_url)
				text = bits[1] if len(bits) == 2 else link_url
				parsed_link = urllib.parse.urlparse(link_url)
				
				# color code link depending on type
				# local -> green; gemini -> blue; http/https -> magenta; other -> gray
				# change in tgmi 1.1.2 to reflect additional test for parsed_link.scheme
				if ((parsed_url.netloc == parsed_link.netloc) and (parsed_link.scheme == 'gemini')):
					lexercode = 'E'
				# tgmi 1.1.4 change to get correct color coding of local files
				#elif ((not cfg.OS_IS_LINUX) and ('file' == parsed_link.scheme)):
				elif ('file' == parsed_link.scheme):
					lexercode = 'E'
				elif (parsed_link.scheme == 'gemini'):
					lexercode = 'F'
				elif ((parsed_link.scheme == 'http') or (parsed_link.scheme == 'https')):
					lexercode = 'G'
				elif (parsed_link.scheme == 'titan'):
					lexercode = 'J'
				else:
					lexercode = 'H'
					
				str1 = "[%d] %s" % (len(cfg.menu), text)
				if (len(cfg.menu) < 10):
					str1 = str1[:4] + ' ' + str1[4:]
					#str1 = ' ' + str1

				bbody += pr_longline(str1, lexercode, '', '     ')

			# header3 lines
			elif line.startswith('###'):
				# tgmi 1.1.3 - strict conform to spec
				# strip any whitespace between *** and beginning of header
				#str1 = line[3:].lstrip()
				bbody += pr_longline(line[4:], 'D', '', '')
				#bbody += pr_longline(str1, 'D', '', '')

			# header2 lines
			elif line.startswith('##'):
				# tgmi 1.1.3 - strict conform to spec
				# strip any whitespace between ** and beginning of header
				str1 = line[2:].lstrip()
				#bbody += pr_longline(line[3:], 'C', '', '')
				bbody += pr_longline(str1, 'C', '', '')

			# header1 lines
			elif line.startswith('#'):
				# old code - allowed whitespace at start of header
				#bbody += pr_longline(line[2:], 'B', '', '')
				#str1 = 'a' + line[2:]
				#str1 = str1.strip()
				#str1 = str1[1:]
				#start = len(str1) - len(str1.strip())
				#str2 = str1.strip()
				#str2 = (' '*start) + (len(str2)*'\u203e')
				#bbody += pr_longline(str1, 'B', '', '')
				#bbody += pr_longline(str2, 'B', '', '')
				##fheader1 = True
				#h1_index = i

				# tgmi 1.1.3 - strict conform to spec
				# strip whitespace between * and beginning of header
				# strip any whitespace at end of header, since we are underlining
				str1 = line[1:].strip()
				str2 = (len(str1)*'\u203e')
				bbody += pr_longline(str1, 'B', '', '')
				bbody += pr_longline(str2, 'B', '', '')
				h1_index = i

			# >< lines (helpfiles)
			elif line.startswith('><'):
				bbody += pr_longline(line[2:], 'A', ' '*cfg.TAB, ' '*cfg.TAB)
				
			# > lines (quotes)
			elif line.startswith('>'):
				str1 = ' '*cfg.TAB
				# tgmi 1.1.3 - strict conform to spec
				# strip whitespace between > and beginning of actual line
				#str2 = textwrap.fill(line[2:], width=999, expand_tabs=True, tabsize=cfg.TAB, replace_whitespace=False)
				str3 = line[1:].lstrip()
				str2 = textwrap.fill(str3, width=999, expand_tabs=True, tabsize=cfg.TAB, replace_whitespace=False)
				bbody += pr_longline(str2, 'I', str1, str1)
				
			# list entries
			elif line.startswith('* '):
				bbody += pr_longline(line[2:], 'A', '  • ', '    ')  
			
			else:
				# treat the line as a text paragraph
				line = textwrap.fill(line, width=999, expand_tabs=True, tabsize=cfg.TAB, replace_whitespace=False)
				bbody += pr_longline(line, 'A', '', '')
					
	else:
		# handle plain text like a series of gemini lines
		body = textwrap.fill(body, width=999, expand_tabs=True, tabsize=cfg.TAB, replace_whitespace=False)
		for line in body.splitlines():
			bbody += pr_longline(line, 'A', '', '')

	# special tweaks for help.gmi
	if (url == ('file://' + cfg.SUPP + 'help.gmi')):
		bbody = bbody.replace('Agreen   - indicates', 'Egreen   - indicates', 1)
		bbody = bbody.replace('Ablue    - indicates', 'Fblue    - indicates', 1)
		bbody = bbody.replace('Amagenta - indicates', 'Gmagenta - indicates', 1)
		bbody = bbody.replace('A          choosing a magenta link', \
										'G          choosing a magenta link', 1)
		bbody = bbody.replace ('Aorange  - indicates', 'Jorange  - indicates', 1)
		bbody = bbody.replace('Agray    - indicates', 'Hgray    - indicates', 1)

	if (bbody[-1] == '\n'):
		bbody = bbody[:-1]
	
	return bbody
	

def make_re_filter():
	cfg.RE_FILTER = '['
	for c in cfg.PROBLEM_CHARS:
		cfg.RE_FILTER += c
	cfg.RE_FILTER += ']'

# tgmi 1.4.0 use str.translate instead of regex
# can replace and kill characters on one call
def make_translate_table():
	killstr = ''
	for i in range(len(cfg.KILL_CHARS)):
		killstr += cfg.KILL_CHARS[i]
		
	# replace all 26 Regional Indicator characters by the alphabetical equivalent
	# extra characters in cfg.REPLACE_CHARS are symbols not handled properly by
	# Windows Terminal which are replaced by the unicode Replacement Character
	instr = ''
	for i in range(len(cfg.REPLACE_CHARS)):
		instr += cfg.REPLACE_CHARS[i]
	outstr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
	for i in range(len(cfg.REPLACE_CHARS) - 26):
		outstr += '\ufffd'

	cfg.TRANSLATE_TABLE = str.maketrans(instr, outstr, killstr)


def filter_utf8(body):
	str1 = body
	# fix for bad terminal utf-8 character printing of bold italics in medusae.space
	# fix needed in both linux and windows
	str1 = str1.replace('𝑨 𝒈𝒆𝒎𝒊𝒏𝒊 𝒅𝒊𝒓𝒆𝒄𝒕𝒐𝒓𝒚', 'A gemini directory')

	# if using mate-terminal, can return here
	# if having linux terminal issues, execute the Windows Terminal line below
	if (cfg.OS_IS_LINUX and not (cfg.F_FILTER_IN_LINUX)):
		return str1
	
	# modifications for Windows Terminal and xterm in Linux 
	# 1. does not handle Variation Selectors
	# 2. does not handle Regional Indicator characters
	# 3. some random problem symbols, probably symbols too new
	else:
		return str1.translate(cfg.TRANSLATE_TABLE)


# test whether a file is an Atom xml feed
def check_atomfeed(mime, body):
	fIsAtomFeed = False
	testtypes = ['application/atom+xml', 'application/xml', 'text/xml', 'text/html']
	mime, mime_opts = cgi.parse_header(mime)
	#log_to_file('utils[546]: mime = ' + mime)
	if (mime not in testtypes):
		return fIsAtomFeed
	
	# body too short to carry any <entry>...</entry> items
	if (len(body) < 80):
		return fIsAtomFeed
	
	# potentially an atom xml file, check for atom feed tag inside body
	newbody = body.decode(mime_opts.get("charset","UTF-8"))

	# tgmi 1.2.1 deleted search for link tag to reflect that
	# link tag could have multiple items inside it
	if ('xmlns="http://www.w3.org/2005/Atom"' in newbody) or ("xmlns='http://www.w3.org/2005/Atom'" in newbody):
		fIsAtomFeed = True
	
	return fIsAtomFeed


# convert an Atom xml file into gemtext
def atom_to_gemini(url, body):

	def atomtag(token):
		return '{http://www.w3.org/2005/Atom}' + token

	# try/except pair to test for parsing errors in file
	try:
		root = ET.fromstring(body)
	except ET.ParseError:
		return 1, 'xml ParseError: exiting...', ''

	if root.tag != atomtag('feed'):
		return 1, 'Not an Atom feed. Exiting...', ''
	
	parsed_url = urllib.parse.urlparse(url)
	indices_object = re.finditer(pattern='/', string=url)
	indices = [index.start() for index in indices_object]
	base = url[:indices[-1]]

	body = ''
	feed_title, feed_id, feed_link, feed_updated, feed_name = '', '', '', '', ''
	feed_entries = []
	for child in root:
		if child.tag == atomtag('title'):
			feed_title = child.text
		elif child.tag == atomtag('entry'):
			feed_entries.append(child)
		elif child.tag == atomtag('id'):
			feed_id = child.text
		elif child.tag == atomtag('updated'):
			feed_updated = child.text
		elif child.tag == atomtag('author'):
			#feed_author = child
			for c in child:
				if c.tag == atomtag('name'):
					feed_name = c.text
		elif child.tag == atomtag('link'):
			feed_link = child.attrib['href']

	body = '(converted from Atom xml to gemtext)\n\n'
	body += f'# {feed_title}\n'
	body += f'  name    :  {feed_name}\n'
	# if (len(feed_id) > 0):
	# 	str1 = feed_id
	# else:
	# 	str1 = feed_link
	# 	ar = feed_link.split('/')
	# 	if (len(ar) == 4):
	# 		str1 = str1[:-len(ar[3])]

	body += f'  link    :  {base}\n'
	body += f'  updated :  {feed_updated}\n'
	body += f'  entries :  {len(feed_entries)}\n\n'

	nlinks = 0
	for entry in feed_entries:
		entry_date = ''
		entry_title = ''
		entry_link = ''
		for child in entry:
			if child.tag == atomtag('updated'):
				entry_date = child.text[:10]
			elif child.tag == atomtag('title'):
				entry_title = child.text
				# if a title for the entry exists, strip leading and ending
				# whitespace and any header indicators from title
				if (entry_title is not None):
					entry_title = entry_title.strip()
					if entry_title[0] == '#': entry_title = entry_title[1:]
					if entry_title[0] == '#': entry_title = entry_title[1:]
					if entry_title[0] == '#': entry_title = entry_title[1:]
					entry_title = entry_title.lstrip()
				else:
					entry_title = '[no title]'

			elif child.tag == atomtag('link'):
				if (('gemini://' in child.attrib['href']) or child.attrib['href'].startswith('//')):
					entry_link = child.attrib['href']
				# need to special case noulin's gitRepositories
				elif ('gemini://gmi.noulin.net/gitRepositories' in base):
					entry_link = base + '/' + child.attrib['href']

		if (len(entry_link) != 0):
			body += f'=> {entry_link} {entry_date} {entry_title}\n'
			nlinks += 1

	if (nlinks > 0):
		return 0, 'conversion from Atom to Gemini successful', body
	else:
		return 1, 'no gemini links found in Atom feed', body
	

# debugging function
def log_to_file(str1):
	if cfg.F_LOGGING:
		f = open(cfg.APP_DIR + '/supp/logs/logfile.txt', 'a')
		f.write(str1 + '\n')
		f.close()
		return
	else:
		return


# convenience logging function - uses inspect to get and print location
# where the logging was done
# fVList toggles printing list elements by row
def tologfile(varname, var, fVList=False):
	if cfg.F_LOGGING:
		f = open(cfg.APP_DIR + '/supp/logs/logfile.txt', 'a')
		# log where we are in the script
		str1 = '\n' +inspect.stack()[1][1] + '  '
		str1 += inspect.stack()[1][3]
		str1 += '[' + str(inspect.stack()[1][2]) + ']:'
		f.write(str1 + '\n')
		
		# log value of a string, int or float
		if isinstance(var, (int, float)):
			f.write('  ' + varname + ' = ' + str(var) + '\n')
			f.close()
			return
			
		# log a string, print variable name and length, contents following
		elif isinstance(var, str):
			lines = var.split('\n')
			lfcount = var.count('\n')
			f.write('  ' + varname + '   string len = ' + str(len(var)) + '   lfs = ' + str(lfcount) + '\n')
			if (var.count('\n') < 1):
				f.write('   \'' + var + '\'\n')
			else:
				for line in lines:
					f.write(line + '\n')
			f.close()
			return 
			
		# log the list as a long string or if fVList==True
		# log each element on it's own row
		elif isinstance(var, list):
			f.write('  ' + varname + '   ' + str(len(var)) + ' elements' + '\n')
			if fVList:
				for item in var:
					f.write('    ' + str(item) + '\n')
			else:
				f.write('     ' + str(var) + '\n')

			f.close()
			return	
		
		# log the variable name and type
		else:
			str1 = str(type(var))
			f.write('  ' + varname + '   ' + str1 + '\n')
			if ('<class ' not in str1):
				f.close()
				return
			else:
				# log each instance variable, one row each
				# getmembers() returns all the members of an object
				for i in inspect.getmembers(var):
					# to remove private and protected
					# functions
					if not i[0].startswith('_'):
						# to remove other methods that do not start with an underscore
						if not inspect.ismethod(i[1]):
							str2 = str(i[1])
							if ((len(str2) > 0) and ('<built-in method' not in str2)):
								str1 = f'    {i[0]:<10} = '
								f.write(str1 + str2 + '\n')
				
				f.close()
				return
	
	else:
		# logfile.txt never opened for writing
		return
