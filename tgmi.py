#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# import global names
import cfg

# local file imports
from utilities_tgmi import *

import cgi
import os, sys
import glob, re, configparser, time
import subprocess
import textwrap
import urllib.parse
from datetime import datetime
from typing import Callable

import signal
import Agunua
import Agunua.status

from prompt_toolkit.application import Application, get_app
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.layout import Layout
from prompt_toolkit.widgets import Box, Frame, TextArea, HorizontalLine
from prompt_toolkit.layout.containers import HSplit, VSplit, Window
from prompt_toolkit.layout.controls import FormattedTextControl
from prompt_toolkit.layout.dimension import LayoutDimension as D
from prompt_toolkit.formatted_text import ANSI, HTML
from prompt_toolkit.layout import FloatContainer, Float
from prompt_toolkit.widgets import Dialog, Label, Button, Frame, Checkbox
from prompt_toolkit.styles import Style
from prompt_toolkit.key_binding.bindings.focus import focus_next
from prompt_toolkit.key_binding.bindings.page_navigation import scroll_page_up, \
				scroll_page_down, scroll_one_line_down, scroll_one_line_up, \
				scroll_forward, scroll_backward
from prompt_toolkit.lexers import Lexer
from prompt_toolkit.styles.named_colors import NAMED_COLORS
from prompt_toolkit.document import Document
from prompt_toolkit.formatted_text.base import StyleAndTextTuples

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# commented out in tgmi 1.1 - causes error
#os.environ['SSLKEYLOGFILE'] = ''

# find character size of terminal
cfg.COLS,cfg.ROWS = re.findall(r'\d+', str(os.get_terminal_size()))
cfg.ROWS = int(cfg.ROWS)
cfg.COLS = int(cfg.COLS)

# check if terminal column width is too narrow
if (cfg.COLS < 70):
	str1 = '\ntgmi requires a minimum terminal column width of 70.\n'
	str1 += 'A terminal column width of at least 85 is recommended\n'
	str1 += 'for the normal 80 column width of gemtext pages.\n'
	print(cstr(str1))
	sys.exit()

# initialization section
set_paths_and_cleanup()

# read settings from settings.conf, if it does not exist, create with default values
read_settings()

# read AUTOSENDCERTS from auto_send_certs.txt, if that file does not exist, 
# create an empty file
read_autosendcerts()

# check that we are not using a windows settings.conf in linux and vice versa
if cfg.OS_IS_LINUX:
	if ('/mnt/' in cfg.PERM_DLPATH):
		str1 = '\nsettings.conf seems to be from a windows WSL install.\n'
		str1 += 'Delete settings.conf in supp/settings and restart.\n'
		str1 += 'Modify new settings.conf as desired.\n'
		print(cstr(str1))
		sys.exit()
else:
	if ('/mnt/' not in cfg.PERM_DLPATH):
		str1 = '\nsettings.conf seems to be from a linux install.\n'
		str1 += 'Delete settings.conf in supp\\settings and restart.\n'
		str1 += 'Modify new settings.conf as desired.\n'
		print(cstr(str1))
		sys.exit()

# make cfg.TRANSLATE_TABLE for windows terminal
make_translate_table()

# check to see if cfg.DISPLAY_WIDTH is larger than actual cfg.COLS
# if so, set cfg.DISPLAY_WIDTH to cfg.COLS-5
cfg.DISPLAY_WIDTH = cfg.COLS-5 if (cfg.COLS<cfg.DISPLAY_WIDTH+5) else cfg.DISPLAY_WIDTH
# cfg.PAD = ' '*int((cfg.COLS-cfg.DISPLAY_WIDTH)/2)

gmiboxheight = cfg.ROWS -4 # 2 frame, 1 command, 1 cmd feedback
# gmiboxpad = cfg.PAD[1:]

# valid one-letter commands
valid_1cmd = {'1', '2', '3', '4', '5', '6', '7', '8', '9', 
					'c', 'q', 'r', 's', 'b', 'B', 'h', 'H', 'I', '!'}

# save original PROMPT_TOOLKIT_COLOR_DEPTH if it exists for restoration on quit
OLD_PROMPT_TOOLKIT_CD = subprocess.check_output('echo $PROMPT_TOOLKIT_COLOR_DEPTH', shell=True, text=True)
# set PROMPT_TOOLKIT_COLOR_DEPTH for use by this program
str1 = os.environ['PROMPT_TOOLKIT_COLOR_DEPTH'] = 'DEPTH_24_BIT'

# initialize variables for use in main action handler loop
# caps = mailcap.getcaps()
cfg.menu,cfg.hist,cfg.lc = [], [], 1
cfg.mime, query_str, expired_url, str_TLSerr = '', '', '', ''
fInput,fExtendedInfo,fAcceptExpired,fIsAtomFeed = False,False,False,False
# for gmi pane line numbering
fLineNumbering = False
# for 's' (show source) commands
last_text_body,last_text_mime = '',''

# client certificate and key
clientcert,clientkey = None,None

# welcome screen is a custom .gmi file to centre the welcome message
# on any terminal screen size
create_welcome_screen(gmiboxheight)

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# utility functions using global variables defined in this file
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

# command line feedback message
def cmd_msg(msg, clr='black'):
	cmdmsg.content = FormattedTextControl('  ' + msg, clr)
			
# set window title
def set_titlestr(new_title):
	# padleft = ' '*int((cfg.COLS-len(new_title))/2)
	# titlefield.content = FormattedTextControl(padleft + new_title)
	if (len(new_title) > (cfg.DISPLAY_WIDTH-1)):
		new_title = new_title[-(cfg.DISPLAY_WIDTH-1):]
	#gmiframe.title = ANSI(cstr(new_title, 'black'))
	gmiframe.title = HTML('<style fg="#808080">' + new_title + '</style>')

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####


##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# lexer for *.gmi files first processed by prelexer
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
class GMILexer(Lexer):
	
	def __init__(self, style: str = "") -> None:

		self.style = cfg.GMISTYLES
		self.keys = list(cfg.GMISTYLES.keys())

	def lex_document(self, document: Document) -> Callable[[int], StyleAndTextTuples]:
		lines = document.lines
		
		def get_line(lineno: int) -> StyleAndTextTuples:
			# each line returns one token consisting of the color code and line text
			# one left space is added to the line to provide a one column left margin
			return [(self.style[lines[lineno][0]], ' ' + lines[lineno][1:])]
			
		return get_line
		
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
##
#  define screen layout
##
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

# load the welcome screen on startup
url = 'file://' + cfg.SUPP + 'welcome.gmi'
parsed_url = urllib.parse.urlparse(url)
header = '20 text/gemini;lang=en'
status, cfg.mime = header.split()
fp = open(parsed_url.path, 'rb')
body = fp.read()
fp.close()
cfg.hist.append(url)
gmidoc = prelexer(body, cfg.mime, url, fExtendedInfo)

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
#  define floats - pop-up dialogs (static dialogs only)
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

##### ##### ##### ##### ##### #####
# dlg1 - short help dialog
##### ##### ##### ##### ##### #####
hwidth = len(max(cfg.HELPSTR.split('\n'), key = len)) + 4
	
def dlg1_handle_btn_Exit():
	root_container.floats.pop()			# remove dlg1
	get_app().layout.focus(cmdfield)		# set focus to cmdfield
	
dlg1 = Box(
			Dialog(
				title = 'Command line inputs',
				body = Label(cfg.HELPSTR),
				buttons=[
					Button(text="Exit", handler=dlg1_handle_btn_Exit),
				],
				width=hwidth,
			),
			height=cfg.ROWS,
			width =cfg.COLS,
		)

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
#  define full screen layout
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

# display area for fetched *.gmi 
gmibox = TextArea(
					text=gmidoc,
					read_only=True,
					scrollbar=True,
					line_numbers=fLineNumbering,
					width=cfg.DISPLAY_WIDTH + 1 + 2 + 4*fLineNumbering,
					height=gmiboxheight,
					lexer=GMILexer(),
					style='bg:#ffffff #000000'
			)
			
gmiframe = Frame(
					body=gmibox,
					style='bg:#ffffff #d0d0d0',
					title = HTML('<style fg="#808080">' + 'welcome.gmi' + '</style>')
				)

##### ##### ##### ##### ##### #####
#  command prompt
##### ##### ##### ##### ##### #####

#  command prompt handler
def cmdfield_accept(buff):
	global url, body, gmiframe, gmibox, cmdfield, cmdmsg, query_str, fInput, \
			 fExtendedInfo, fAcceptExpired,  clientcert, clientkey, expired_url, \
			 last_text_body, last_text_mime
	
	# get command
	cmd = (cmdfield.text).strip()
	cmd_msg('')
	fLocalFile = False

	# check if no text before enter
	if (len(cmd) == 0):
		cmd_msg('enter a command or URL, q to quit, h to list commands', 'red')
		return
		
	if (len(cmd) == 1):
			
		if (not (cmd in valid_1cmd)):
			cmd_msg('invalid command', 'red')
			return
	
		elif (cmd == 'q'):
			# clean up any files created in cfg.TEMP
			oldfiles = glob.glob(cfg.TEMP + '*')
			for f in oldfiles:
				os.unlink(f)
			# restore original PROMPT_TOOLKIT_COLOR_DEPTH
			str1 = os.environ['PROMPT_TOOLKIT_COLOR_DEPTH'] = OLD_PROMPT_TOOLKIT_CD
			get_app().exit()
			return
			
		elif (cmd == 'c'):
			cmd_msg('')
			return
			
		elif (cmd == 'h'):
			cmdmsg.content = FormattedTextControl('')

			root_container.floats.append(
				Float(
					content=dlg1,
				)  # optionally pass width and height.
			)
			get_app().layout.focus(dlg1)
			return
		
		##### ##### ##### ##### ##### ##### ##### #####
		# I - create a new self-signed crt/key pair
		##### ##### ##### ##### ##### ##### ##### #####
		elif (cmd == 'I'):
			cmdmsg.content = FormattedTextControl('')

			str1 = cfg.hist[-1] if cfg.hist[-1].startswith('gemini://') else ''
			dlg9_input1 = TextArea(
				text=str1,
				multiline=False
			)

			dlg9_input2 = TextArea(
							text='9999',
							multiline=False,
							width=6
					)
			
			dlg9_checkbox = Checkbox(
							text='Add certificate to auto_send_certs.txt',
							checked=False
					)

			def dlg9_handle_btn_New():
				if (0 < len(dlg9_input1.text)):
					cert_url = dlg9_input1.text
					if ('gemini://' in cert_url):
						cert_url = cert_url[9:]
					if (cert_url[-1] == '/'):
						cert_url = cert_url[:-1]
					if (5 < len(dlg9_input2.text)):
						cert_life = 99999
					elif ( 0 == len(dlg9_input2.text)):
						cert_life = 1
					else:
						cert_life = int(dlg9_input2.text)
					fInput = True
					fErr, msg = create_permanent_cert(cert_url, 'geminiclient', cert_life)

					if (not fErr):
						if dlg9_checkbox.checked:
							redo_autosendcerts(True, cert_url)
							msg += '  (autosend=True) '
						else:
							redo_autosendcerts(False, cert_url)
							msg += '  (autosend=False)'
					
					get_app().layout.focus(cmdfield)		# set focus to cmdfield
					dlg9_input1.text = ''					# reset input1
					dlg9_input2.text = '9999'				# reset input2
					root_container.floats.pop()			# remove dlg9
					get_app().layout.focus(cmdfield)		# set focus to cmdfield
					get_app().style = cfg.tgmiStyle1
					cmdfield_accept('')
					cmdfield.text = ''
					cmd_msg(msg, 'red') if fErr else cmd_msg(msg)
					return
				
				else:
					cmd_msg('cancelled - gemini URL not entered', 'red')
					dlg9_input1.text = ''					# reset input1
					dlg9_input2.text = '9999'				# reset input2
					root_container.floats.pop()			# remove dlg9
					get_app().layout.focus(cmdfield)		# set focus to cmdfield
					return
				
			def dlg9_handle_btn_Replace():
				if (0 < len(dlg9_input1.text)):
					cert_url = dlg9_input1.text
					if ('gemini://' not in cert_url):
						cert_url = 'gemini://' + dlg9_input1.text
					parsed_cert_url = urllib.parse.urlparse(cert_url)
					surl = parsed_cert_url.netloc + parsed_cert_url.path
					surl = surl.replace('/', '@@')
					certdir = cfg.CERTS + surl + '/'
					#log_to_file('tgmi[343]: certdir = ' + certdir)
					# if path exists, delete directory and contents and call dlg9_handle_btn_New
					if os.path.exists(certdir):
						output = subprocess.run(['rm', '-r', certdir], capture_output=True)
						#log_to_file('tgmi[344] certdir = ' + certdir)
					dlg9_handle_btn_New()

				else:
					cmd_msg('cancelled - gemini URL not entered', 'red')
					dlg9_input1.text = ''					# reset input1
					dlg9_input2.text = '9999'				# reset input2
					root_container.floats.pop()			# remove dlg9
					get_app().layout.focus(cmdfield)		# set focus to cmdfield
					return

			def dlg9_handle_btn_Cancel():
				#global dlg9_input1, dlg9_input2
				cmd_msg('cancelled, no certificate created', 'red')
				# line below removed in tgmi 1.1 - causes error
				dlg9_input1.text = ''						# reset input1
				dlg9_input2.text = '9999'					# reset input2
				root_container.floats.pop()				# remove dlg9
				get_app().layout.focus(cmdfield)			# set focus to cmdfield
				return

			str1 = ' Enter a gemini url including subdirectory (if any) for certificate use:\n'
			hwidth = len(max(str1.split('\n'), key = len))
			dlg9_btn_New = Button(text="New", handler=dlg9_handle_btn_New)
			dlg9_btn_Replace = Button(text="Replace", handler=dlg9_handle_btn_Replace)
			dlg9_btn_Cancel = Button(text="Cancel", handler=dlg9_handle_btn_Cancel)

			dlg9 = Box(
						Dialog(
							title = 'Create and store new self-signed client certificate',
							#body=Label(str1),
							body=HSplit(
								[
									Label(str1),
									dlg9_input1,
									Label(''),
									VSplit(
										[
											Label(6*' ' + 'Certificate life in days (default=' + dlg9_input2.text + ', max=99999):'),
											dlg9_input2,
											Label(5*' '),
										],
										width=hwidth+6),
									Label(''),
									VSplit(
										[
											Label(10*' '),
											dlg9_checkbox,
											Label(10*' '),
										],
										width=hwidth+6),
								]
							),
							buttons=[
								dlg9_btn_New,
								dlg9_btn_Replace,
								dlg9_btn_Cancel
							],
							width=hwidth+8,
						),
						height=cfg.ROWS,
						width =cfg.COLS,
					)
			
			root_container.floats.append(
				Float(
					content=dlg9,
				)  # optionally pass width and height.
			)
			get_app().layout.focus(dlg9)
			return
		
		# end cmd 'I' code
		##### ##### ##### ##### ##### ##### ##### #####

		elif (cmd == 'H'):
			cmdmsg.content = FormattedTextControl('')
			url = 'file://' + cfg.SUPP + 'help.gmi'

		elif (cmd == 'B'):
			cmdmsg.content = FormattedTextControl('')
			url = 'file://' + cfg.SUPP + 'bookmarks.gmi'
			
		elif (cmd == 'b'):
			# sanity check #1 check if we are at the welcome page
			if ((len(cfg.hist) == 1) and (cfg.hist[0] == ('file://' + cfg.SUPP + 'welcome.gmi'))):
				cmd_msg('nothing to bookmark', 'red')
				return
			# sanity check #2 check if we are trying to bookmark bookmarks.gmi
			elif (cfg.hist[-1] == 'file://' + cfg.SUPP + 'bookmarks.gmi'):
				cmd_msg('can\'t bookmark the bookmarks page', 'red')
				return
				
			# sanity check #3 check if we are trying to bookmark help.gmi
			elif (cfg.hist[-1] == 'file://' + cfg.SUPP + 'help.gmi'):
				cmd_msg('can\'t bookmark the help page', 'red')
				return
				
			# display dlg2 - bookmarks dialog to get name for new bookmark
			else:
				cmdmsg.content = FormattedTextControl('')
				
				# create dlg2 - bookmarks dialog (dynamic content)
				def dlg2_handle_btn_OK():
					if (0 < len(dlg2_input.text)):
						# name string returned, update bookmarks.gmi
						if (dlg2_input.text=='@'):
							str1 = ''
							str3 = '(unnamed link)'
						else:
							str1 = ' ' + dlg2_input.text
							str3 = dlg2_input.text
						str2 = '=> ' + cfg.hist[-1] + str1
						bkfile = open(cfg.SUPP + 'bookmarks.gmi', 'a')
						bkfile.write(str2 + '\n')
						bkfile.close()
						cmd_msg(f' new bookmark added: "{str3}"', 'blue')
						
					else:
						cmd_msg('cancelled - no name entered for bookmark', 'red')
						
					root_container.floats.pop()			# remove dlg2
					get_app().layout.focus(cmdfield)		# set focus to cmdfield
					
				def dlg2_handle_btn_Cancel():
					cmd_msg('cancelled - new bookmark not created', 'red')
					root_container.floats.pop()			# remove dlg2
					get_app().layout.focus(cmdfield)		# set focus to cmdfield
					
				dlg2_input = TextArea(
									text='',
									multiline=False,
									#password=password,
									#completer=completer,
									#validator=validator,
									#accept_handler=accept,
								)
				str1 = 'URL: ' + cfg.hist[-1]
				str1 += ' '*(48 - min(48,len(str1)))
				str1 += '\n\nEnter name for link (enter @ for unnamed:\n'

				dlg2 = Box(
							Dialog(
								title = 'New Bookmark',
								body=HSplit(
									[
										Label(str1),
										dlg2_input,
										Label('')
									]
								),
								buttons=[
									Button(text="OK", handler=dlg2_handle_btn_OK),
									Button(text="Cancel", handler=dlg2_handle_btn_Cancel),
								]
							),
							height=cfg.ROWS,
							width =cfg.COLS,
						)
				root_container.floats.append(
						Float(content=dlg2)
					)
				get_app().layout.focus(dlg2)
				return
			
		# r - return to previous URL
		elif (cmd == "r"):
			if (len(cfg.hist)<=2):
				cmd_msg('at first page, no more history', 'red')
				return
				
			else:
				# first pop removes current url, second pop retrieves previous url
				fExtendedInfo = False
				url = cfg.hist.pop()
				url = cfg.hist.pop()
				
		# s - open non-blocking text editor process to view page source
		#     if bookmarks.gmi is open, open in editor without displaying the URL at top of page
		elif (cmd == 's'):
			# first check we are not at an ExtendedInfo page
			if (body[:16] == '# Info for URL: '):
				cmd_msg('no source - URL info page', 'red')
				return
			# second check that we have a HANDLER_TEXT
			elif (cfg.HANDLER_TEXT == '*none*'):
				cmd_msg('no text handler specified in settings.conf', 'red')
				return
				
			# handle source of a remote gemini file
			elif (cfg.hist[-1][:7] != 'file://'):
				path = cfg.TEMP + 'body.gmi'
				fbody = open(path, 'wb')
				str1 = 'URL: ' + cfg.hist[-1]
				str1 += '\n' + ('='*len(str1)) + '\n'
				# convert str1 to binary
				str1 = str1.encode('utf-8')
				body = last_text_body
				# _, mime_opts = cgi.parse_header(cfg.mime)
				# ubody = body.decode(mime_opts.get("charset","UTF-8"), errors='replace')
				# fbody.write(str1 + '\n' + ubody)
				fbody.write(body)
				fbody.close()
				handler = cfg.HANDLER_TEXT
				if (not cfg.OS_IS_LINUX):
					path = make_winpath(cfg.TEMP + 'body.gmi')
					handler = get_win_name(cfg.HANDLER_TEXT)
				cmd_msg(f'source displayed by handler {handler}', 'blue')
				subprocess.Popen([cfg.HANDLER_TEXT, path], \
								stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
				return
				
			# handle a local gemini file
			else:
				handler = cfg.HANDLER_TEXT
				path = cfg.hist[-1][7:]
				if (not cfg.OS_IS_LINUX):
					path = make_winpath(path)
					handler = get_win_name(cfg.HANDLER_TEXT)
				cmd_msg(f'source displayed by handler {handler}', 'blue')
				subprocess.Popen([cfg.HANDLER_TEXT, path], \
								stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
				return
				
		# numeric entry - map entry on current page
		elif cmd.isnumeric():
			# sanity check: see if integer is one of the numbered urls on the page
			if ((int(cmd)-1) not in range(len(cfg.menu))):
				cmd_msg('no link number ' + cmd, 'red')
				return
			else:
				url = cfg.menu[int(cmd)-1]
				
		# ! - info request for a displayed gemini link
		elif (cmd == '!'):
			# sanity check1 - test whether we have a *.gmi in hist
			# or are still on the welcome page
			if (len(cfg.hist) <= 1):
				cmd_msg('at welcome page not an external gemini link', 'red')
				return
			# sanity check2 - test whether last page in history was gemini://...
			elif (cfg.hist[-1][:9] != 'gemini://'):
				cmd_msg('last page viewed not an external gemini link', 'red')
				return
				
			# passed sanity checks, get the url
			else:
				fExtendedInfo = True
				url = cfg.hist[-1]
		
		else:
			cmd_msg('invalid command', 'red')
			return
			

	if (len(cmd) >= 2):
		
		# numeric entry - map entry on current page
		if cmd.isnumeric():
			# sanity check: see if integer is one of the numbered urls on the page
			if ((int(cmd)-1) not in range(len(cfg.menu))):
				cmd_msg('no link number ' + cmd, 'red')
				return
			else:
				url = cfg.menu[int(cmd)-1]
		
		# !<str> - extended info request on gemini link <str>
		elif (cmd[0] == '!'):
			# expecting everything after ! to be a valid gemini url
			fExtendedInfo = True
			url = cmd[1:]
			if not "://" in url:
				url = "gemini://" + url
			# no return here as we want to drop down to parsed_url = ...
		
		# at this point we expect a string containing a url
		else:
			url = cmd
			if not "://" in url:
				url = "gemini://" + url

	# if we made it to here we should be dealing with an actual url
	parsed_url = urllib.parse.urlparse(url)
	
	# if we are moving to a new gemini url, reset clientcert, clientkey
	parsed_url_old = urllib.parse.urlparse(cfg.hist[-1])
	#tologfile('parsed_url', parsed_url)

	if ((parsed_url.scheme == 'gemini') and (parsed_url.netloc != parsed_url_old.netloc)):
		#log_to_file('tgmi[653]: parsed_url.netloc = ' + parsed_url.netloc)
		#log_to_file('           parsed_url.path   = ' + parsed_url.path + '\n')
		# tgmi 1.3.2 amendment to test that url matches a complete entry in cfg.AUTOSENDCERTS
		if ((parsed_url.netloc + parsed_url.path) in cfg.PARSED_ASC):
			surl = parsed_url.netloc + parsed_url.path
			surl = surl.replace('/', '@@')
			certbase = cfg.SUPP + 'certs/'
			certdir = certbase + surl + '/'
			
			fErr = False
			if not os.path.exists(certdir):
				cmd_msg('AUTOSENDCERTS err: no cert/key pair directory found', 'red')
				return
			else:
				files = os.listdir(certdir)
				if (len(files)<2):
					fErr = True
				else:
					if (('sscertificate.crt' == files[1]) and ('sscertificate.key' == files[0])):
						clientcert = certdir + files[1]
						clientkey = certdir + files[0]
					elif (('crt' in files[0]) and ('key' in files[1])):
						clientcert = certdir + files[0]
						clientkey = certdir + files[1]
					elif (('crt' in files[1]) and ('key' in files[0])):
						clientcert = certdir + files[1]
						clientkey = certdir + files[0]
					else:
						fErr = True
						
				if fErr:
					cmd_msg('AUTOSENDCERTS err: no cert/key pair found in directory', 'red')
					return
				
		else:
			clientcert,clientkey = None,None
		
		# if new url and we have not just authorised viewing a site with
		# an expired certificate reset fAcceptExpired and expired_url
		if (expired_url != url):
			fAcceptExpired = False
			expired_url = ''
	
	# if scheme is 'http' or 'https' use HANDLER_WWW
	if ((parsed_url.scheme == 'http') or (parsed_url.scheme == 'https')):
		if (cfg.HANDLER_WWW !='*none*'):
			subprocess.Popen([cfg.HANDLER_WWW, url], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
			str1 = get_win_name(cfg.HANDLER_WWW)
			cmd_msg(f'link passed to handler {str1}', 'blue')
			return
		else:
			cmd_msg('no web browser specified in settings.conf', 'red')
			return
		
	# if scheme is 'file' and suffix is '.gmi' display the file
	elif ((parsed_url.scheme == 'file') and (parsed_url.path[-4:] == '.gmi')):
		fLocalFile = True

	# if scheme is 'titan' display a dialog to upload a file via titan
	elif (parsed_url.scheme == 'titan'):
		if (url == 'titan://fake4679315.link'):
			cmd_msg('I did say this was a fake link!', 'red')
			return

		cmdmsg.content = FormattedTextControl('')
				
		# dlg10 - Titan file upload
		dlg10_input = TextArea(
			text='',
			multiline=False,
			scrollbar=False,
			dont_extend_height=False,
		)
		
		def dlg10_handle_btn_OK():
			global url
			if (0 < len(dlg10_input.text)):
				filepath = dlg10_input.text
				if not os.path.exists(filepath):
					cmd_msg('not found: "' + filepath + '"', 'red')
					dlg10_input.text = ''
					root_container.floats.pop()			# remove dlg10
					get_app().layout.focus(cmdfield)		# set focus to cmdfield
					return

				# we have a valid filepath, check for file size limits
				fsize = os.path.getsize(filepath)
				if ((parsed_url.netloc == 'bbs.geminispace.org') and (fsize > 102400)):
					cmd_msg('filesize (' + str(fsize) + ') exceeds 100KB limit of bbs.geminispace.org; not uploaded', 'red')
					dlg10_input.text = ''
					root_container.floats.pop()				# remove dlg10
					get_app().layout.focus(cmdfield)			# set focus to cmdfield
					return 

				# we have a valid filepath, try to upload it
				_, fname = os.path.split(filepath)
				dlg10_input.text = ''
				if (filepath[-4:] == '.gmi'):
					fmime = 'text/gemini'
				else:
					output = subprocess.run(['xdg-mime', 'query', 'filetype', filepath], capture_output=True)
					fmime = output.stdout.decode().split('\n')[0]

				try:
					output = subprocess.run(['bash', 'titan-upload.sh', '-c', clientcert, \
			      	'-k', clientkey, '-t', fname, '-m', fmime, url, filepath], capture_output=True)
				except:
					cmd_msg('titan-upload.sh: problem uploading file, check supp/logs/logfile.txt')
					log_to_file('\nException raised on titan upload of file \'' + fname + '\'')
					str_array = output.stdout.decode().split('\n')
					for item in str_array:
						log_to_file('   ' + str(item))
					str_array = output.stderr.decode().split('\n')
					for item in str_array:
						log_to_file('   ' + str(item))

				result = output.stdout.decode()
				#log_to_file(result)
				root_container.floats.pop()				# remove dlg10
				get_app().layout.focus(cmdfield)			# set focus to cmdfield
				# bbs.geminispace.org uses '30' to indicate success, can also use '20'
				if ((result[:2]=='20') or (result[:2]=='30')):
					cmd_msg('file "' + fname + '" uploaded to draft', 'blue')
				else:
					print(result)
				#cmdfield_accept('')
				#cmdfield.text = ''
				return
			
			else:
				cmd_msg('No file specified, titan upload cancelled.', 'red')
				dlg10_input.text = ''
				root_container.floats.pop()				# remove dlg10
				get_app().layout.focus(cmdfield)			# set focus to cmdfield
				return
			
		def dlg10_handle_btn_Cancel():
			cmd_msg('titan file upload cancelled', 'red')
			root_container.floats.pop()					# remove dlg10
			get_app().layout.focus(cmdfield)				# set focus to cmdfield
			return
			
		str1 = 'Enter filename or full/relative path and filename if file\n'
		str1 += 'is not in "' + cfg.APP_DIR + '"\n'
		
		dlg10 = Box(
					Dialog(
						title = 'File upload using Titan',
						body=HSplit(
							[
								Label(str1),
								dlg10_input,
								Label('')
							]
						),
						buttons=[
							Button(text="OK", handler=dlg10_handle_btn_OK),
							Button(text="Cancel", handler=dlg10_handle_btn_Cancel),
						]
					),
					height=cfg.ROWS,
					width =cfg.COLS,
				)

		root_container.floats.append(
			Float(
				content=dlg10,
			)  # optionally pass width and height.
		)
		get_app().layout.focus(dlg10_input)
		#return
	
		# end titan file upload section
		##### ##### ##### ##### ##### ##### ##### ##### ##### #####
		return

		
	# if scheme not gemini, file, http, https, titan
	elif ((parsed_url.scheme != "gemini") and (not fLocalFile)):
		cmd_msg(parsed_url.scheme + ' links not handled by ' + cfg.PNAME, 'red')
		return
	
	# local *.gmi files are read and move directly to text processing
	if fLocalFile:
		header = '20 text/gemini;lang=en'
		status, cfg.mime = header.split()
		
		if (not fExtendedInfo):
			fp = open(parsed_url.path, 'rb')
			body = fp.read()
			fp.close()
			str_TLSerr = ''

	# set up server timeout alarm and use Agunua to retrieve 
	# data from a Gemini server
	else:
		signal.signal(signal.SIGALRM, server_timeout)
		signal.alarm(cfg.SERVER_TIMEOUT)
			
		try:
			u = Agunua.GeminiUri(url, 
									insecure=True,
									get_content=True,
									parse_content=False,
									maxlines=0,
									maxsize=0,
									binary=True,
									follow_redirect=True,
									tofu=cfg.TOFU,
									force_ipv4=cfg.F_IPV4_ONLY,
									force_ipv6=False,
									connect_to=None,
									accept_expired=fAcceptExpired,
									debug=False,
									clientcert=clientcert,
									clientkey=clientkey)
									
			# print out the components of u if fExtendedInfo=True
			# then return control to command line
			if fExtendedInfo:
				signal.alarm(0)
				body = ExtendedInfo(url, u)
			
			cfg.mime = u.meta
						
			# status code is '10' - input required
			# construct a new url containing the input and send back for processing
			if (u.status_code == '10'):
				# display dlg3 - normal input dialog 
				# alarm will be restarted when the search string url is submitted
				signal.alarm(0)
				cmdmsg.content = FormattedTextControl('')
				
				# dlg3 - server input requested
				# bbs.geminispace.org gets multiline input dialogs
				fMultiline = True if ((parsed_url.netloc == 'bbs.geminispace.org') \
					and not ('/delete/' in url)) else False
				dlg3_input = TextArea(
					text='',
					multiline=fMultiline,
					scrollbar=fMultiline,
					dont_extend_height=fMultiline,
					height=10 + 9*fMultiline
				)
				
				def dlg3_handle_btn_OK():
					global url
					query_str = ''
					if (0 < len(dlg3_input.text)):
						query_str = dlg3_input.text
						cmd_msg(f'submitted: query_str = {query_str}', 'blue')
						dlg3_input.text = ''
						url += "?" + urllib.parse.quote(query_str)
						fInput = True
						query_str = ''
						root_container.floats.pop()			# remove dlg3
						cmdfield.text = url
						get_app().layout.focus(cmdfield)		# set focus to cmdfield
						cmdfield_accept('')
						cmdfield.text = ''
						return
					
					else:
						cmd_msg('cancelled - nothing entered', 'red')
						dlg3_input.text = ''
						root_container.floats.pop()			# remove dlg3
						get_app().layout.focus(cmdfield)		# set focus to cmdfield
						return
					
					
				def dlg3_handle_btn_Cancel():
					global query_str, dlg3_input
					cmd_msg('cancelled', 'red')
					# line below removed in tgmi 1.1 - causes error
					#dlg3_input.text = ''
					root_container.floats.pop()				# remove dlg3
					get_app().layout.focus(cmdfield)			# set focus to cmdfield
					return
					
				str1 = cfg.mime + ':'
				str1 += ' '*(50 - min(50,len(str1))) + '\n'
				
				dlg3 = Box(
							Dialog(
								title = 'Input requested by server',
								body=HSplit(
									[
										#Label(cfg.mime + ':\n'),
										Label(str1),
										dlg3_input,
										Label('')
									]
								),
								buttons=[
									Button(text="OK", handler=dlg3_handle_btn_OK),
									Button(text="Cancel", handler=dlg3_handle_btn_Cancel),
								]
							),
							height=cfg.ROWS,
							width =cfg.COLS,
						)

				root_container.floats.append(
					Float(
						content=dlg3,
					)  # optionally pass width and height.
				)
				get_app().layout.focus(dlg3_input)
				
			# status code is '11' - sensitive input required
			# construct a new url containing the input and send back for processing
			elif (u.status_code == '11'):
				# display dlg4 - sensitive input dialog
				# alarm will be restarted when the search string url is submitted
				signal.alarm(0)
						
				# dlg4 - server input requested
				def dlg4_handle_btn_OK():
					global url
					query_str = ''
					if (0 < len(dlg4_input.text)):
						query_str = dlg4_input.text
						cmd_msg(f'submitted: query_str = {query_str}', 'blue')
						dlg4_input.text = ''
						url += "?" + urllib.parse.quote(query_str)
						fInput = True
						query_str = ''
						root_container.floats.pop()			# remove dlg4
						cmdfield.text = url
						get_app().layout.focus(cmdfield)		# set focus to cmdfield
						get_app().style = cfg.tgmiStyle1
						cmdfield_accept('')
						cmdfield.text = ''
						return
					
					else:
						cmd_msg('cancelled - nothing entered', 'red')
						dlg4_input.text = ''
						
						root_container.floats.pop()			# remove dlg4
						get_app().layout.focus(cmdfield)		# set focus to cmdfield
						get_app().style = cfg.tgmiStyle1
						return
					
				def dlg4_handle_btn_Cancel():
					global query_str, dlg3_input
					cmd_msg('cancelled', 'red')
					dlg4_input.text = ''
					root_container.floats.pop()				# remove dlg4
					get_app().layout.focus(cmdfield)			# set focus to cmdfield
					get_app().style = cfg.tgmiStyle1
					return
					
				dlg4_input = TextArea(
									text='',
									multiline=False,
									password=True,
								)

				str1 = cfg.mime + ':'
				str1 += ' '*(50 - min(50,len(str1)))
				str1 += '\n(Note: text is sent in the clear to the server like a normal query)\n'
				
				dlg4 = Box(
							Dialog(
								title = 'Sensitive (hidden) input requested by server',
								body=HSplit(
									[
										#Label(cfg.mime + ':\n'),
										Label(str1),
										dlg4_input,
										Label('')
									]
								),
								buttons=[
									Button(text="OK", handler=dlg4_handle_btn_OK),
									Button(text="Cancel", handler=dlg4_handle_btn_Cancel),
								]
							),
							height=cfg.ROWS,
							width =cfg.COLS,
						)

				get_app().style = cfg.tgmiStyle2
				root_container.floats.append(
					Float(
						content=dlg4,
					)  # optionally pass width and height.
				)
				get_app().layout.focus(dlg4_input)
				
			# status code is '60' - request for client certificate
			elif (u.status_code == '60'):
				#log_to_file('[0819] u.status.code=' + str(u.status_code) + ' url=' + url)
				# Prompt
				# alarm will be restarted when the certificate is submitted
				signal.alarm(0)
				
				# server has requested a client certificate but we don't know if
				# the server requires an ephemeral certificate, an authorized certificate
				# or a registered self-signed certificate
				# check to see if we have a stored certificate before asking whether we
				# should generate an ephemeral certificate
				parsed_url = urllib.parse.urlparse(url)
				surl = parsed_url.netloc + parsed_url.path
				surl = surl.replace('/', '@@')
				certbase = cfg.SUPP + 'certs/'
				certdir = certbase + surl + '/'
				
				fErr = False
				if os.path.exists(certdir):
					files = os.listdir(certdir)
					if (len(files)<2):
						fErr = (True)
					else:
						if (('sscertificate.crt' == files[1]) and ('sscertificate.key' == files[0])):
							clientcert = certdir + files[1]
							clientkey = certdir + files[0]
						elif (('crt' in files[0]) and ('key' in files[1])):
							clientcert = certdir + files[0]
							clientkey = certdir + files[1]
						elif (('crt' in files[1]) and ('key' in files[0])):
							clientcert = certdir + files[1]
							clientkey = certdir + files[0]
						else:
							fErr = True
							
					if (not fErr):
						cmdfield.text = url
						get_app().layout.focus(cmdfield)		# set focus to cmdfield
						get_app().style = cfg.tgmiStyle1
						cmdfield_accept('')
						cmdfield.text = ''
						return
						
					else:
						cmd_msg('cert store path exists but no cert/key pair found', 'red')
						return
				
				# dlg6 - client certificate required
				dlg6_input = TextArea(
								text='1',
								multiline=False,
								width=6
						)
			
				def dlg6_handle_btn_Yes():
					global url, clientcert, clientkey
					# yes response - need to generate an ephemeral certificate
					# and send this certificate to the requesting server
					if (5 < len(dlg6_input.text)):
						cert_life = 99999
					elif (0 == len(dlg6_input.text)):
						cert_life = 1
					else:
						cert_life = int(dlg6_input.text)
					fErr, msg = generate_ephcert('geminiclient', cert_life)
					fInput = True
					clientcert = cfg.TEMP + 'sscertificate.crt'
					clientkey = cfg.TEMP + 'sscertificate.key'
					cmdfield.text = url
					root_container.floats.pop()			# remove dlg6
					get_app().layout.focus(cmdfield)		# set focus to cmdfield
					get_app().style = cfg.tgmiStyle1
					cmdfield_accept('')
					cmdfield.text = ''
					cmd_msg(msg, 'red') if fErr else cmd_msg(msg)
					return
						
				def dlg6_handle_btn_No():
					cmd_msg('server client certificate request denied, no access', 'red')
					root_container.floats.pop()				# remove dlg6
					get_app().layout.focus(cmdfield)			# set focus to cmdfield
					get_app().style = cfg.tgmiStyle1
					return
					
				str1 = 'Server: ' + str(u.meta) + '\n\n'
				str1 += 'The server has requested a client certificate to access this page.\n'
				str1 += 'Generate and send an ephemeral certificate as the response?\n\n'
				str1 += 'Use command \'I\' to generate and store a permanent certificate.\n' 
				hwidth = len(max(str1.split('\n'), key = len))
				btn_Yes = Button(text="Yes", handler=dlg6_handle_btn_Yes)
				btn_No = Button(text="No", handler=dlg6_handle_btn_No)
					
				dlg6 = Box(
							Dialog(
								title = 'Client certificate required',
								#body=Label(str1),
								body=HSplit(
									[
										Label(str1),
										VSplit(
											[
												Label(6*' ' + 'Certificate life in days (default=1, max=99999):'),
												dlg6_input,
												Label(5*' '),
											],
											width=hwidth+6),
									]
								),
								buttons=[
									btn_Yes,
									btn_No
								],
								width=hwidth+8,
							),
							height=cfg.ROWS,
							width =cfg.COLS,
						)

				get_app().style = cfg.tgmiStyle1
				root_container.floats.append(
					Float(
						content=dlg6,
					)  # optionally pass width and height.
				)
				get_app().layout.focus(btn_Yes)

			# status code is '61' - certificate presented is unauthorized
			elif (u.status_code == '61'):
				# Prompt
				# alarm will be restarted when the certificate is submitted
				signal.alarm(0)
				#str1 = '  status code: ' + u.status_code + '  [' + cfg.gemcodes[u.status_code] + ']'
				str1 = str(u.meta)
				cmd_msg(str1, 'red')
				
				parsed_url = urllib.parse.urlparse(url)
				surl = parsed_url.netloc + parsed_url.path
				surl = surl.replace('/', '@@')
				certbase = cfg.SUPP + 'certs/'
				certdir = certbase + surl + '/'
				
				fErr = False
				if os.path.exists(certdir):
					files = os.listdir(certdir)
					if (len(files)<2):
						fErr = True
					else:
						if (('sscertificate.crt' == files[1]) and ('sscertificate.key' == files[0])):
							clientcert = certdir + files[1]
							clientkey = certdir + files[0]
						elif (('crt' in files[0]) and ('key' in files[1])):
							clientcert = certdir + files[0]
							clientkey = certdir + files[1]
						elif (('crt' in files[1]) and ('key' in files[0])):
							clientcert = certdir + files[1]
							clientkey = certdir + files[0]
						else:
							fErr = True
							
					if (not fErr):
						cmdfield.text = url
						get_app().layout.focus(cmdfield)		# set focus to cmdfield
						get_app().style = cfg.tgmiStyle1
						cmdfield_accept('')
						cmdfield.text = ''
						return

				else:
					# str1 = 'Server: ' + str(u.meta) + '\n\n'
					str1 = 'The server has requested an authorized client certificate.\n'
					str1 += 'There are no stored certificates for this page.\n'
					str1 += 'If you have a certificate and key for this page, place them\n'
					str1 += "inside a folder named '" + surl + "'\n"
					str1 += 'Located in: ' + certbase + '\n'
				
					# dlg7 - authorized client certificate required
					def dlg7_handle_btn_Exit():
						global url, clientcert, clientkey
						clientcert,clientkey = None,None
						root_container.floats.pop()			# remove dlg7
						get_app().layout.focus(cmdfield)		# set focus to cmdfield
						get_app().style = cfg.tgmiStyle1
						return
						
					dlg7 = Box(
								Dialog(
									title = 'Server replies: ' + str(u.meta),
									body=Label(str1),
									buttons=[
										Button(text="Exit", handler=dlg7_handle_btn_Exit)
									]
								),
								height=cfg.ROWS,
								width =cfg.COLS,
							)
					
					get_app().style = cfg.tgmiStyle2
					root_container.floats.append(
						Float(
							content=dlg7,
						)  # optionally pass width and height.
					)
					get_app().layout.focus(dlg7)
					return


			# status code is '62' - certificate presented is invalid
			elif (u.status_code == '62'):
				# Prompt
				# alarm will be restarted when the certificate is submitted
				signal.alarm(0)
				if u.status_code in cfg.gemcodes.keys():
					str1 = 'status code: ' + u.status_code + '  [' + cfg.gemcodes[u.status_code] + ']'
				cmd_msg(str1, 'red')
				return
				
				
			# check for expired certificate error, dlg option to accept
			elif ((hasattr(u, 'expired')) and (u.expired) and (not fAcceptExpired)):
				# alarm will be restarted when url is resubmitted
				signal.alarm(0)
				
				# dlg8 - server certificate has expired
				def dlg8_handle_btn_Yes():
					global url, fAcceptExpired, fInput, expired_url
					fInput = True		# to have same url called again
					fAcceptExpired = True
					expired_url = url
					cmdfield.text = url
					root_container.floats.pop()			# remove dlg8
					get_app().layout.focus(cmdfield)		# set focus to cmdfield
					get_app().style = cfg.tgmiStyle1
					cmdfield_accept('')
					cmdfield.text = ''
					return
						
				def dlg8_handle_btn_No():
					cmd_msg('server certificate has expired', 'red')
					root_container.floats.pop()				# remove dlg8
					get_app().layout.focus(cmdfield)			# set focus to cmdfield
					get_app().style = cfg.tgmiStyle1
					return

				str1 = parsed_url.netloc + '- certificate has expired\n'
				str1 += 'Do you wish to try to see the page?\n\n'
					
				dlg8 = Box(
							Dialog(
								title = 'Expired server certificate',
								body=Label(str1),
								buttons=[
									Button(text="Yes", handler=dlg8_handle_btn_Yes),
									Button(text="No", handler=dlg8_handle_btn_No),
								]
							),
							height=cfg.ROWS,
							width =cfg.COLS,
						)

				get_app().style = cfg.tgmiStyle2
				root_container.floats.append(
					Float(
						content=dlg8,
					)  # optionally pass width and height.
				)
				get_app().layout.focus(dlg8)
				
				
		except ServerTimeoutError:
			str1 = f'server timeout--stop after waiting {cfg.SERVER_TIMEOUT} seconds'
			cmd_msg(str1, 'red')
			signal.alarm(0)
			return
		
		except Agunua.InvalidUri:
			cmd_msg(f'invalid URI {url}', 'red')
			signal.alarm(0)
			return
			
		except Agunua.NonGeminiUri as e:
			if (f'{e}' == 'titan'):
				# we dont' want to handle ':' as a switch to use titan
				# give a warning message instead
				#str1 = 'Non-Gemini URI scheme is "{}"   url="{}"'.format(e, url)
				#log_to_file('tgmi[1203]: ' + str1)
				if ('bbs.geminispace.org' in url):
					cmd_msg('Use titan only through Compose draft->Add long text/Add image', 'red')
				else:
					cmd_msg('titan cannot be used here, aborting', 'red')
				signal.alarm(0)
				return
			
			# Agunua.NonGeminiUri is not 'titan', give general error message 
			else:
				cmd_msg('unhandled URI scheme detected, aborting', 'red')
				signal.alarm(0)
				return
		
		# turn off timeout alarm since we have received a response
		signal.alarm(0)
			
		# invalid URL
		if (u.error[-20:]=='not known or invalid'):
			cmd_msg('invalid or unknown URL', 'red')
			return
			
		# changed server certificate
		elif (u.network_success == False) and (str(u.error)[:20] == 'Former public key at'):
			# dlg5 - new server certificate
			def dlg5_handle_btn_Yes():
				global url
				# yes response - new certificate accepted
				cmd_msg(f'new certificate stored for {parsed_url.netloc}', 'blue')
				#delete TOFU certificate from fingerprints and reload url
				subprocess.run(['rm', cfg.TOFU + parsed_url.netloc])
				fInput = True		# to have same url called again with new cert becoming TOFU
				root_container.floats.pop()			# remove dlg5
				cmdfield.text = url
				get_app().layout.focus(cmdfield)		# set focus to cmdfield
				get_app().style = cfg.tgmiStyle1
				cmdfield_accept('')
				cmdfield.text = ''
				return
					
			def dlg5_handle_btn_No():
				cmd_msg(f'{parsed_url.netloc} not trusted', 'red')
				root_container.floats.pop()				# remove dlg5
				get_app().layout.focus(cmdfield)			# set focus to cmdfield
				get_app().style = cfg.tgmiStyle1
				return

			with open(cfg.TOFU + parsed_url.netloc, 'r') as f:
				storedCert = f.read()
			f.close()
			storedCert = storedCert.splitlines()

			stored_cdate = storedCert[1][:10] + ' ' + storedCert[1][11:-1]
			dt_cdate = datetime.strptime(stored_cdate, '%Y-%m-%d %H:%M:%S')

			dt_today = datetime.today()
			str1 = f'Certificate change for server: {parsed_url.netloc}'
			str3 = '\nDo you want to update this server\'s certificate?'
			
			if (dt_today < dt_cdate):
				str2 = f'\nThe stored certificate\'s expiry date is {stored_cdate}.'
			else:
				str2 = f'\nThe stored certificate expired on {stored_cdate}.'
				
			dlg5 = Box(
						Dialog(
							title = 'Server certificate change',
							body=Label(str1 + str2 + str3),
							buttons=[
								Button(text="Yes", handler=dlg5_handle_btn_Yes),
								Button(text="No", handler=dlg5_handle_btn_No),
							]
						),
						height=cfg.ROWS,
						width =cfg.COLS,
					)

			get_app().style = cfg.tgmiStyle2
			root_container.floats.append(
				Float(
					content=dlg5,
				)  # optionally pass width and height.
			)
			get_app().layout.focus(dlg5)
						
		# status code not 20 - success
		elif (u.status_code != '20'):
			# cmd_msg(str(u.network_success) + '  ' + str(u.error))
			# return
			if (u.status_code != None):
				if u.status_code in cfg.gemcodes.keys():
					str1 = 'status code: ' + u.status_code + '  [' + cfg.gemcodes[u.status_code] + ']'
				else:
					str1 = 'status code: ' + u.status_code + ' - undefined status code'
			else:
				str1 = str(u.error)
				# log_to_file('[1053] u.error = ' + str1)
			cmd_msg(str1, 'red')
			return
			
		# if we made it to here we should have a payload from the server response
		# try to extract the payload (the Agunua request was to send in binary)
		if (not fExtendedInfo):
			body = u.payload
			# added in tgmi 1.1.1 to append indication that no TLS close was received
			if ('(lack of TLS close?)' in u.error):
				str_TLSerr = ' (no TLS close)'
			else:
				str_TLSerr = ''
		
		# check for Agunua unwillngness to retrieve payload
		if (body == None):
			cmd_msg('Agunua reports: ' + str(u.error), 'red')
			return
		
	# check to see if we are dealing with an atom feed
	fIsAtomFeed = check_atomfeed(cfg.mime, body)
	if fIsAtomFeed:
		# save original text in last_text_body so can download using 's' command
		last_text_body = body
		err, str1, body = atom_to_gemini(url, body)
		if (err):
			cmd_msg(str1, 'red')
			return
		header = '20 text/gemini'
		status, cfg.mime = header.split()

	# display body text
	if cfg.mime.startswith("text/"):
		cfg.lc = 0
		parsed_url = urllib.parse.urlparse(url)
		if fLocalFile:
			new_title = parsed_url.path.rsplit("/", 1)[-1]
		else:
			new_title = parsed_url.netloc + parsed_url.path
		set_titlestr(new_title)
		
		if not fIsAtomFeed:
			last_text_body = body
			last_text_mime = cfg.mime

		# preprocess text to get true linecounts and add lexercodes to beginning of each line
		gmidoc = prelexer(body, cfg.mime, url, fExtendedInfo)
		gmibox.text = gmidoc
		if fExtendedInfo:
			fExtendedInfo = False
			str_TLSerr = ''
		cmd_msg(f'formatted lines in page = {cfg.lc}' + str_TLSerr, '#808080')
		str_TLSerr = ''
		cfg.hist.append(url)
		return
	
	# Handle non-text
	else:
		# get original name of file as saved on the gemini server
		fname = url.split('/')[-1]
		# store fname in cfg.TEMP
		dlfile = open(cfg.TEMP + fname, 'wb')
		dlfile.write(body)
		dlfile.close()
		mimetype = u.meta.split(';')[0]

		# if mimetype is application/octet-stream move it to permanent download path
		if (mimetype == 'application/octet-stream'):
			subprocess.run(['mv', dlfile.name, cfg.PERM_DLPATH + fname])
			cmd_msg('no handler, \'' + fname + '\' saved to ' + cfg.PERM_DLPATH, 'blue')
			return
		
		if (mimetype == 'application/rss+xml'):
			cmd_msg('mimetype \'application/rss+xml\' not handled by tgmi', 'red')
			return
					
		if cfg.OS_IS_LINUX:
			cmd_msg(f"file '{fname}' sent to {mimetype} handler", 'blue')
			output = subprocess.run(['xdg-open', dlfile.name], capture_output=True)
			if (output.returncode == 0):
				return
			
			else:
				# if xdg-open reports an error (eg no handler), save the file and notify
				# tgmi 1.1 change: capture output to suppress text being sent to command line
				output = subprocess.run(['mv', dlfile.name, cfg.PERM_DLPATH + fname])
				cmd_msg('no handler, \'' + fname + '\' saved to ' + cfg.PERM_DLPATH, 'blue')
				return
				
		# cfg.OS_IS_LINUX==False ie we are using WSL or WSL2
		else:

			try:
				# try giving the file to windows and letting it find a default handler
				path = make_winpath(dlfile.name)
				output = subprocess.run(['cmd.exe', '/C', 'start', path], \
								stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
				str1 = f"file '{fname}' sent to default handler"
				
			except:
				output = subprocess.run(['mv', dlfile.name, cfg.PERM_DLPATH + fname], \
							stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
				str1 = "no handler, '" + fname + "' saved to " + cfg.PERM_DLPATH
			
			cmd_msg(str1, 'blue')
			return

#  end - command prompt handler

#  command prompt TextArea
cmdfield = TextArea(
		height=1,
		prompt="command > ",
		#style="class:input-field",
		multiline=False,
		wrap_lines=False,
		accept_handler=cmdfield_accept,
		width=cfg.DISPLAY_WIDTH + 5 + 4*fLineNumbering,
		style='bg:#ffffff #000000'
	 )

##### ##### ##### ##### ##### #####
#  end command prompt
##### ##### ##### ##### ##### #####
	 
# display area for command prompt feedback
cmdmsg = Window(
					content=FormattedTextControl(''),
					height=D.exact(1),
					width=cfg.DISPLAY_WIDTH + 5 + 4*fLineNumbering,
					style='bg:#ffffff #000000'
				)

# the root container
root_container = FloatContainer(
	content = HSplit(
		[
			Box(body=gmiframe, style='bg:#e0e0e0'),	# pane for .gmi files
			Box(body=cmdfield,style='bg:#e0e0e0'),		# command line
			Box(body=cmdmsg, style='bg:#e0e0e0')		# command feedback message
		]
	),
	floats= [ ]
)

layout = Layout(container=root_container, focused_element=cmdfield)

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
#  key bindings
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
kb = KeyBindings()

@kb.add("c-q")
def _(event):
	"Quit when control-q is pressed."
	event.app.exit()
	
@kb.add("pageup")
def _(event):
	w = event.app.layout.current_window
	event.app.layout.focus(gmibox)
	scroll_page_up(event)
	event.app.layout.focus(w)

@kb.add("pagedown")
def _(event):
	w = event.app.layout.current_window
	event.app.layout.focus(gmibox)
	scroll_page_down(event)
	event.app.layout.focus(w)
	
@kb.add("up")
def _(event):
	w = event.app.layout.current_window
	event.app.layout.focus(gmibox)
	scroll_one_line_up(event)
	event.app.layout.focus(w)

@kb.add("down")
def _(event):
	w = event.app.layout.current_window
	event.app.layout.focus(gmibox)
	scroll_one_line_down(event)
	event.app.layout.focus(w)
	
	
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
def main():
	# Build a main application object.
	application = Application(
		layout=layout,
		key_bindings=kb,
		style=cfg.tgmiStyle1,
		enable_page_navigation_bindings=True,
		mouse_support=True,
		full_screen=True,
	)
	
	application.run()


if __name__ == "__main__":
	main()
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
