#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# shared globals for use by tpgmi and utilities_tgmi
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

from prompt_toolkit.styles import Style
from prompt_toolkit.styles.named_colors import NAMED_COLORS

# the program name
PNAME = 'tgmi'    # t_erminal  *.gmi
# program version number and date of last change
PVERSION		= '1.4.0'
PDATE			= '2023-07-10'

# gemini status codes
gemcodes = {
				"10": "Input request",
				"11": "Sensitive input request",
				"20": "Success",
				"30": "Temporary redirect",
				"31": "Permanent redirect",
				"40": "Temporary failure",
				"41": "Server unavailable",
				"42": "CGI error",
				"43": "Proxy error",
				"44": "Slow down",
				"50": "Permanent failure",
				"51": "Not found",
				"52": "Gone",
				"53": "Proxy request refused",
				"59": "Bad request",
				"60": "Client certificate required",
				"61": "Certificate not authorised",
				"62": "Certificate not valid"
			}

APP_DIR			= ''
SUPP				= ''
SETTINGS			= ''
TEMP				= ''
TOFU				= ''
LOGS				= ''
ROWS				= 0
COLS				= 0
PAD				= ''

F_LOGGING		= True			# used only for debugging
F_OS_IS_LINUX	= True

##### ##### ##### ##### #####
# tgmi 1.4.0
# set flag to true if using xterm in Linux or other Linux 
# terminal program which has utf-8 display issues
# if using mate-terminal can remain false
F_FILTER_IN_LINUX = False
##### ##### ##### ##### #####
			
# define global variables set in settings.conf
SERVER_TIMEOUT		= 8
TAB					= 4			# define tabsize as 4 spaces
PERM_DLPATH			= ''
DISPLAY_WIDTH		= 80
HANDLER_TEXT		= ''
HANDLER_WWW			= ''
F_IPV4_ONLY			= False

# define the AUTOSENDCERTS and PARSED_ASC global variables
AUTOSENDCERTS = ''
PARSED_ASC = []

# global line counter for screen
lc = 0

# history of visited URLs is global
hist = []

# url menu from displayed page
menu = []

mime = ''

# dialog style
tgmiStyle1 = Style.from_dict({ 
				'bg'							:	'bg:#ffffff',
				'dialog'						:	'bg:#ffffff',
				'dialog frame.label'		:	'bg:#f0f0f0 #000000',
				'dialog.body'				:	'bg:#f0f0f0 #000000', # light grey background
				'dialog shadow'			:	'bg:#e0e0e0',
				'button'						:	'bg:#d0e2f1 #000000', # bluish button
				'button button.focused'	:	'bg:#3c7cea #ffffff',
})

tgmiStyle2 = Style.from_dict({
				'dialog'						:	'bg:#fdfdfd',
				'dialog frame.label'		:	'bg:#ffe0e0 #000000',
				'dialog.body'				:	'bg:#ffe0e0 #000000', # pink background
				'dialog shadow'			:	'bg:#fdfdfd',
				'button'						:	'bg:#ffb0b0 #000000', # red button
				'button button.focused'	:	'bg:#ef2929 #ffffff',
})

# styling for *.gmi rendering
GMISTYLES = {
					'A'	:	'fg:#000000',							# normal text
					'B'	:	'fg:MediumBlue bold',				# H1 header
					'C'	:	'fg:MediumBlue bold',				# H2 header
					'D'	:	'fg:MediumBlue',						# H3 header
					'E'	:	'fg:#008000',							# local gemini link
					'F'	:	'fg:MediumBlue',						# external gemini link
					'G'	:	'fg:DarkViolet',						# http, https link
					'H'	:	'fg:#808080',							# unhandled link
					'I'	:	'fg:#000000 italic',					# quote
					'J'	:	'fg:#ce5c00'							# titan link
				}

# tgmi 1.4.0 VARIATION SELECTOR-15, VARIATION SELECTOR-16 & ZERO WIDTH JOINER to be removed
KILL_CHARS = [ '\ufe0e', '\ufe0f', '\u200d' ]

# tgmi 1.4.0
# regional indicator characters to be replaced by capitalized latin alphabet character
# wheel, ring buoy, headstone, placard to be replaced by \ufffd "Replacement Character"
REPLACE_CHARS = [
				'\U0001f1e6', '\U0001f1e7', '\U0001f1e8', '\U0001f1e9', '\U0001f1ea', 
				'\U0001f1eb', '\U0001f1ec', '\U0001f1ed', '\U0001f1ee', '\U0001f1ef', 
				'\U0001f1f0', '\U0001f1f1', '\U0001f1f2', '\U0001f1f3', '\U0001f1f4', 
				'\U0001f1f5', '\U0001f1f6', '\U0001f1f7', '\U0001f1f8', '\U0001f1f9', 
				'\U0001f1fa', '\U0001f1fb', '\U0001f1fc', '\U0001f1fd', '\U0001f1fe',
				'\U0001f1ff', 
				'\U0001f6de', '\U0001f6df', '\U0001faa6', '\U0001faa7'
				]

TRANSLATE_TABLE = []

# contents of help dialog
HELPSTR =	'q - (quit) exits the program\n' \
				'c - (clear) clears commmand response message line\n' \
				'r - (return) returns to the previous page viewed\n' \
				's - (source) displays source in a text editor\n' \
				'b - (bookmark) add current page to bookmarks.gmi\n' \
				'B - (bookmarks page) displays the bookmarks.gmi page\n' \
				'h - (help) lists all keyboard commands (this dialog)\n' \
				'H - (all help) displays Help page\n' \
				'I - (identity) create a new self-signed certificate\n' \
				'! - info about the current gemini link (see Help page)\n\n' \
				'number  - go to link [number] listed on current page\n' \
				'<link>  - go to link\n' \
				'!<link> - extended info for <link> (see Help page)\n' \
				'ctrl-q  - emergency quit\n\n' \
				'Page scrolling:\n' \
				'    pageup/pagedown  - page scroll\n' \
				'    \u2191/\u2193, mouse wheel - one line scroll\n\n' \
				'(Press \'Enter\' or \'Space\' to close)'

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# list of prompt-toolkit dialogs
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# dlg1		short help dialog (static)							 - h command
# dlg2		bookmarks dialog										 - b command
# dlg3		server input requested (usually for a search) - response to 10
# dlg4		server input requested - sensitive				 - response to 11
# dlg5		changed server certificate notification
# dlg6		request for a client certificate					 - response to 60
# dlg7		authorized client certificate required			 - response to 61
# dlg8		notification of expired server certificate
# dlg9		create new self-signed client certificate			I command
# dlg10		titan file upload
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
