#!/bin/bash
# create a new self-signed private key 'sscertificate.key' and a
# self-signed certificate 'sscertificate.crt' using openssl
# type: elliptic curve using prime256v1
# crt and key generated in pem format as files in supp/temp subfolder

# arg $1 - /CN={issuer name}
# arg $2 - {days from now until expiry}

cd supp/temp/
openssl req -new -subj $1 -x509 -newkey ec -pkeyopt ec_paramgen_curve:prime256v1 -days $2 -nodes -out sscertificate.crt -keyout sscertificate.key
openssl x509 -noout -in sscertificate.crt -issuer
openssl x509 -noout -in sscertificate.crt -dates

# the line below will generate a public key 'sspubkey.key' from the private key
#openssl ec -in sscertificate.key -pubout -out sspubkey.key
